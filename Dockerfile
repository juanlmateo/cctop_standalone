FROM python:3.8

RUN wget https://sourceforge.net/projects/bowtie-bio/files/bowtie/1.2.3/bowtie-1.2.3-linux-x86_64.zip

RUN unzip bowtie-1.2.3-linux-x86_64.zip -d opt/

RUN rm bowtie-1.2.3-linux-x86_64.zip

ENV PATH=/opt/bowtie-1.2.3-linux-x86_64:$PATH

RUN pip install bx-python

COPY setup.py README.md pkg/

COPY cctop/ pkg/cctop/

RUN pip install pkg/

RUN rm -rf setup.py README.md cctop/

WORKDIR /data/
